extends Camera2D

onready var target
const CAM_MOVE_SPEED = 0.3
func _ready():
	pass

func _physics_process(delta):
	if target:
		var p = global_position
		var t = target.global_position
		global_position.x = lerp(p.x, t.x, CAM_MOVE_SPEED)
		global_position.y = lerp(p.y, t.y, CAM_MOVE_SPEED)