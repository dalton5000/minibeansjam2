extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const WALK_SPEED = 65
const RUN_SPEED = 90
const MOUNT_SPEED = 3

const ACCELERATION = 0.035 
const DECELERATION = 0.2


var running = false

var about_to_die = false

var registered_dinos = []

var mountable = true
var mounted = false


var stunned = false
var stunned_since = 99999
var stunned_timeout = 3


#const ACCELERATION = 0.02
#const DECELERATION = 0.08
var direction = Vector2(0,0)
var velocity = Vector2(0,0)
onready var player = get_node("/root/Main/Player/Player")

enum STATES{
	IDLE,
	CHASING_PLAYER,
	CHASING_HERBIVORE,
	CHASING_BAIT,
	FLEEING,
	STUNNED,
	MOUNTED,
	RESCUED
	}

var state = IDLE

var behaviour = "idle"
var current_animation = null
var next_animation
onready var WalkAnimation = get_node("WalkAnimation")
var begin = Vector2()
var end = Vector2()
var path = []
var find_random_point_delay = false
var update_behaviour_delay = false
onready var WalkRandomlyTimer = get_node("WalkRandomlyTimer")
onready var StunAnimation = get_node("StunAnimation")
onready var BehaviourTimer = get_node("BehaviourTimer")
onready var DirectionTimerChill = get_node("DirectionTimerChill")
onready var DirectionTimerChase = get_node("DirectionTimerChase")
onready var DetectionArea = get_node("PlayerDetectionArea")
onready var RescueZone = get_node("/root/Main/Level/RescueZone")
onready var AggroParticles = get_node("AggroParticles")
onready var FleeingParticles = get_node("FleeingParticles")
onready var BaitParticles = get_node("BaitParticles")
onready var navigation = get_node("/root/Main/Level/DefaultNavigation")
onready var carnivore = has_node("Carnivore")
onready var herbivore = has_node("Herbivore")
onready var water = has_node("Water")
var last_seen_herbivore
var target_dino
var last_seen_bait
var target_bait
var last_seen_carnivore
var last_seen_dino
signal about_to_die
var direction_delay = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.


func _physics_process(delta):
	next_animation = "idle"
#	direction = Vector2(0,0)
	if !update_behaviour_delay:
		update_behaviour()
		
	match state:
		STUNNED:
			stunned_since +=delta
			if stunned_since > stunned_timeout:
				end_stunned()
				
		MOUNTED:
			handle_mounted_input(delta)
			
		IDLE:
			begin = global_position
			if !find_random_point_delay:
				end = Vector2(global_position.x+rand_range(-50,50),global_position.y+rand_range(-50,50))
				find_random_point_delay = true
				WalkRandomlyTimer.start()
			direction = navigate(delta)
			move_and_animate(direction)
			
		CHASING_PLAYER:
			begin = global_position
			end = player.global_position
			direction = navigate(delta)
			move_and_animate(direction)
			
		CHASING_HERBIVORE:
			begin = global_position
			end = target_dino.global_position
			direction = navigate(delta)
			move_and_animate(direction)
			
		CHASING_BAIT:
			begin = global_position
			end = target_bait.global_position
			direction = navigate(delta)
			move_and_animate(direction)
			
		FLEEING:
			begin = global_position
			if herbivore:
				end = global_position+((last_seen_carnivore.global_position - global_position)*-1)
			elif water:
				end = global_position+((last_seen_dino.global_position - global_position)*-1)
			direction = navigate(delta)
			move_and_animate(direction)
			
		RESCUED:
			begin = global_position
			if !find_random_point_delay:
				end = Vector2(global_position.x+rand_range(-30,30),global_position.y+rand_range(-30,30))
				find_random_point_delay = true
				WalkRandomlyTimer.start()
			#TODO: new navpoly inside area
			direction = navigate(delta)
			move_and_animate(direction)
	
func switch_states(new_state):
	if new_state != state:
		match new_state:
			IDLE:
				state = IDLE
				AggroParticles.emitting = false
				FleeingParticles.emitting = false
				BaitParticles.emitting = false
				running = false
				$RiderSprite.visible=false
				
			CHASING_PLAYER:
				state = CHASING_PLAYER
				$Sfx/AggroSound.play()
				AggroParticles.emitting = true
				FleeingParticles.emitting = false
				BaitParticles.emitting = false
				running = false
				
			CHASING_HERBIVORE:
				state = CHASING_HERBIVORE
				$Sfx/AggroSound.play()
				AggroParticles.emitting = true
				FleeingParticles.emitting = false
				BaitParticles.emitting = false
				running = false
				
			CHASING_BAIT:
				state = CHASING_BAIT
				$Sfx/AggroSound.play()
				AggroParticles.emitting = false
				FleeingParticles.emitting = false
				BaitParticles.emitting = true
				running = false
				
			FLEEING:
				state = FLEEING
				$Sfx/PanicSound.play()
				AggroParticles.emitting = false
				FleeingParticles.emitting = true
				BaitParticles.emitting = false
				running = true
				
			MOUNTED:
				
				state = MOUNTED
				AggroParticles.emitting = false
				FleeingParticles.emitting = false
				BaitParticles.emitting = false
				running = false
				$RiderSprite.visible=true
				
			STUNNED:
				if state == MOUNTED:
					player.unmount()
				state = STUNNED
				AggroParticles.emitting = false
				FleeingParticles.emitting = false
				BaitParticles.emitting = false
				running = false
				stunned_since = 0.0
				StunAnimation.play("stun")
				
			RESCUED:
				state = RESCUED
				AggroParticles.emitting = false
				FleeingParticles.emitting = false
				BaitParticles.emitting = false
				running = false
				$RiderSprite.visible=false


func hit_trap(trap_type, trap_object):
	if trap_type == "basic":
		get_stunned()
		trap_object.disabled = true
	if trap_type == "veggie_bait":
		if herbivore or water:
			$Sfx/EatSound.play()
			trap_object.disabled = true
			#consume bait
	elif trap_type == "meat_bait":
		if carnivore:
			$Sfx/EatSound.play()
			trap_object.disabled = true
			#consume bait
	update_behaviour()

func get_stunned():
	switch_states(STUNNED)
	
func end_stunned():
	switch_states(IDLE)
	
	
func collide_with_body(body):
	if carnivore:
		if body == player:
			body.get_hit()
		elif body.has_node("Herbivore") and not state in [STUNNED, MOUNTED, RESCUED]:
			body.get_stunned()
			switch_states(IDLE)
	elif herbivore or water:
		pass

func dies():
	about_to_die = true
	emit_signal("about_to_die", self)
	if state == MOUNTED:
		player.unmount()
	queue_free()

func _on_target_dino_about_to_die(dying_dino):
	if target_dino == dying_dino:
		update_behaviour()

func _update_path():
	var p = navigation.get_simple_path(begin, end, true)
	path = Array(p) # PoolVector2Array too complex to use, convert to regular array
	path.invert()

func move_and_animate(direction):
	direction = direction.normalized()
	if direction == Vector2(0,0):
#		velocity = lerp(velocity, direction, DECELERATION)
		velocity = Vector2(0,0)
	else:
		if direction.x >= 0.5:
			next_animation = "walk_right"
		elif direction.x <= -0.5:
			next_animation = "walk_left"
		elif direction.y >= 0.5:
			next_animation = "walk_down"
		elif direction.y <= -0.5:
			next_animation = "walk_up"
#		velocity = lerp(velocity, direction, ACCELERATION)
		if !running:
			velocity = direction*WALK_SPEED
		else:
			velocity = direction*RUN_SPEED
		
	if current_animation != next_animation:
		if next_animation == "idle":
			WalkAnimation.stop()
		else:
			current_animation = next_animation
			if current_animation == "walk_right" or current_animation == "walk_left":
				get_node("CollisionShapeUp").disabled = true
				get_node("CollisionShapeDown").disabled = true
				get_node("CollisionShapeLeftRight").disabled = false
			elif current_animation == "walk_up":
				get_node("CollisionShapeUp").disabled = false
				get_node("CollisionShapeDown").disabled = true
				get_node("CollisionShapeLeftRight").disabled = true
			else: #down
				get_node("CollisionShapeUp").disabled = true
				get_node("CollisionShapeDown").disabled = false
				get_node("CollisionShapeLeftRight").disabled = true
			WalkAnimation.play(current_animation)
		
	move_and_slide(velocity)
	for i in range(get_slide_count()):
		var collision = get_slide_collision(i)
		collide_with_body(collision.collider)

func navigate(delta):
	if !direction_delay:
		_update_path()
		if path.size() > 1:
			var to_walk = delta * WALK_SPEED
			while to_walk > 0 and path.size() >= 2:
				var pfrom = path[path.size() - 1]
				var pto = path[path.size() - 2]
				var d = pfrom.distance_to(pto)
				direction = pto - global_position
				if d <= 1:
					direction = Vector2(0,0)
				if d <= to_walk:
					path.remove(path.size() - 1)
					to_walk -= d
				else:
					path[path.size() - 1] = pfrom.linear_interpolate(pto, to_walk/d)
					to_walk = 0
			
			var atpos = path[path.size() - 1]
			#global_position = atpos
			
		if path.size() < 2:
			path = []
		direction_delay = true
		if state in [CHASING_PLAYER, CHASING_HERBIVORE]:
			DirectionTimerChase.start()
		else:
			DirectionTimerChill.start()
	else:
		if path.size() > 1:
			var to_walk = delta * WALK_SPEED
			var pfrom = path[path.size() - 1]
			var pto = path[path.size() - 2]
			var d = pfrom.distance_to(pto)
			if d <= 1:
				direction = Vector2(0,0)
			if d <= to_walk:
				path.remove(path.size() - 1)
				to_walk -= d
			else:
				path[path.size() - 1] = pfrom.linear_interpolate(pto, to_walk/d)
				to_walk = 0
	return direction
		
	

func _on_WalkRandomlyTimer_timeout():
	find_random_point_delay = false

func _on_BehaviourTimer_timeout():
	update_behaviour_delay = false

func update_behaviour():
	BehaviourTimer.stop()
	last_seen_herbivore = null
	last_seen_carnivore = null
	last_seen_dino = null
	target_dino = null
	last_seen_bait = null
	target_bait = null
	var nearby_bodies = DetectionArea.get_overlapping_bodies()
	var nearby_areas = DetectionArea.get_overlapping_areas()
	
	match state:
		STUNNED:
			pass
		MOUNTED:
			pass
		RESCUED:
			if !RescueZone.overlaps_body(self):
				switch_states(IDLE)
			continue
		_:
			if carnivore:
				for area in nearby_areas:
					if area.has_node("Meat") and !area.disabled:
						last_seen_bait = area
				if last_seen_bait:
					if target_bait != last_seen_bait:
							target_bait = last_seen_bait
							switch_states(CHASING_BAIT)
				elif nearby_bodies.has(player):
					if RescueZone.overlaps_body(self):
						switch_states(RESCUED)
					else:
						switch_states(CHASING_PLAYER)
				else:
					for body in nearby_bodies:
						if body.has_node("Herbivore"):
							if not body.about_to_die:
								last_seen_herbivore = body
					if !last_seen_herbivore:
						if RescueZone.overlaps_body(self):
							switch_states(RESCUED)
						else:
							switch_states(IDLE)
					elif target_dino != last_seen_herbivore:
						if RescueZone.overlaps_body(self):
							switch_states(RESCUED)
						else:
							target_dino = last_seen_herbivore
							if not target_dino in registered_dinos:
								register_dino(target_dino)
							switch_states(CHASING_HERBIVORE)
			elif herbivore:
				for area in nearby_areas:
					if area.has_node("Veggie") and !area.disabled:
						last_seen_bait = area
				for body in nearby_bodies:
					if body.has_node("Carnivore"):
						last_seen_carnivore = body
				if !last_seen_carnivore:
					if last_seen_bait:
						if target_bait != last_seen_bait:
								target_bait = last_seen_bait
								switch_states(CHASING_BAIT)
					else:
						if RescueZone.overlaps_body(self):
							switch_states(RESCUED)
						else:
							switch_states(IDLE)
				else:
					if RescueZone.overlaps_body(self):
						if last_seen_bait:
							if target_bait != last_seen_bait:
									target_bait = last_seen_bait
									switch_states(CHASING_BAIT)
						else:
							switch_states(RESCUED)
					else:
						switch_states(FLEEING)
			elif water:
				for area in nearby_areas:
					if area.has_node("Veggie") and !area.disabled:
						last_seen_bait = area
				for body in nearby_bodies:
					if body.has_node("Carnivore") or body.has_node("Herbivore"):
						last_seen_dino = body
				if !last_seen_dino:
					if last_seen_bait:
						if target_bait != last_seen_bait:
								target_bait = last_seen_bait
								switch_states(CHASING_BAIT)
					else:
						if RescueZone.overlaps_body(self):
							switch_states(RESCUED)
						else:
							switch_states(IDLE)
				else:
					if RescueZone.overlaps_body(self):
						if last_seen_bait:
							if target_bait != last_seen_bait:
									target_bait = last_seen_bait
									switch_states(CHASING_BAIT)
						else:
							switch_states(RESCUED)
					else:
						switch_states(FLEEING)
						
	update_behaviour_delay = true
	BehaviourTimer.start()
	
func register_dino(dino):
	dino.connect("about_to_die",self,"_on_target_dino_about_to_die")
	registered_dinos.append(dino)
	
func mount():
	mounted = true
	switch_states(MOUNTED)
	
	if has_node("Water"):
		get_node("/root/Main/Level/WaterBorders").global_position = Vector2(-100000,-100000)
	

func unmount():
	mounted = false

	if RescueZone.overlaps_body(self):
		switch_states(RESCUED)
	else:
		switch_states(IDLE)
		
	if has_node("Water"):
#		set_collision_mask_bit(10,true)
		get_node("/root/Main/Level/WaterBorders").global_position = Vector2(0,0)

func start_swimming():
	print("swim start")
	var tex = ImageTexture.new()
	tex.load("res://gfx/sprites/bluedino-swimming-sheet.png")
	get_node("Sprite").texture = tex

func stop_swimming():
	print("swim stop")
	var tex = ImageTexture.new()
	tex.load("res://gfx/sprites/bluedino-sheet.png")
	get_node("Sprite").texture = tex
	

func handle_mounted_input(delta):
	var target_speed = Vector2(0,0)
	
	if Input.is_action_pressed("ui_right"):
		next_animation = "walk_right"
		target_speed.x = 1
	elif Input.is_action_pressed("ui_left"):
		next_animation = "walk_left"
		target_speed.x = -1
	else:
		target_speed.x = 0
	if Input.is_action_pressed("ui_up"):
		next_animation = "walk_up"
		target_speed.y = -1
	elif Input.is_action_pressed("ui_down"):
		next_animation = "walk_down"
		target_speed.y = 1
	else:
		target_speed.y = 0
		
	if target_speed == Vector2(0,0):
		velocity.x = lerp(velocity.x, target_speed.x, DECELERATION)
		velocity.y = lerp(velocity.y, target_speed.y, DECELERATION)
	else:
		target_speed = target_speed.normalized()*WALK_SPEED
		velocity.x = lerp(velocity.x, target_speed.x*MOUNT_SPEED, ACCELERATION)
		velocity.y = lerp(velocity.y, target_speed.y*MOUNT_SPEED, ACCELERATION)
	if current_animation != next_animation:
		current_animation = next_animation
		if current_animation == "walk_right" or current_animation == "walk_left":
			get_node("CollisionShapeDown").disabled = true
			get_node("CollisionShapeUp").disabled = true
			get_node("CollisionShapeLeftRight").disabled = false
		elif current_animation == "walk_up" or current_animation == "walk_down":
			get_node("CollisionShapeUp").disabled = false
			get_node("CollisionShapeDown").disabled = false
			get_node("CollisionShapeLeftRight").disabled = true
		if !current_animation == "idle":
			WalkAnimation.play(current_animation)
		else:
			WalkAnimation.stop()
	move_and_slide(velocity)

func _on_VisibilityNotifier2D_screen_entered():
	set_physics_process(true)


func _on_VisibilityNotifier2D_screen_exited():
	set_physics_process(false)


func _on_DirectionTimerChill_timeout():
	direction_delay = false


func _on_DirectionTimerChase_timeout():
	direction_delay = false
