extends Timer

export(float,0,600) var min_time = 1
export(float,0,600) var max_time = 10


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	init_rand()

func start(bla = 0.0):
	init_rand()
	.start()
	
func init_rand():
	set_wait_time(rand_range(min_time,max_time))