extends CanvasLayer

var max_dinos = 0
var saved_dinos = 0

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_SettingsButton_pressed():
	$SettingsWindow.popup()

func select_trap(type):
	match type:
		"trap":
			$AnimationPlayer.play("select_beartrap")
		"meat":
			$AnimationPlayer.play("select_meatbait")
		"veggie":
			$AnimationPlayer.play("select_veggiebait")
			
			