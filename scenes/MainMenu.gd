extends Control

onready var settings_menu = $SettingsWindow
var game_scene = preload("res://scenes/Main.tscn")
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_StartButton_pressed():
	if not $StartSound.playing:
		$StartSound.play()


func _on_SettingsButton_pressed():
	settings_menu.popup()

func _on_StartSound_finished():
	get_tree().change_scene_to(game_scene)
