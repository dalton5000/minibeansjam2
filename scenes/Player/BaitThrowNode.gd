extends Node2D

signal animation_finished

func play(anim):
	$BaitThrowAnimation.play(anim)

func _on_BaitThrowAnimation_animation_finished(anim_name):
	emit_signal("animation_finished")
	queue_free()