extends KinematicBody2D


onready var cam = get_node("/root/Main/Camera/Camera5000")

var mounted = false
var current_mount = null

var about_to_throw = false
var throwing_type = ""
var started_throwing = 9999
var throw_starting_timeout = 0.1
var last_direction = ""
var placing_trap = false

const DEATH_TIMEOUT = 0.2


const WALK_SPEED = 130
const ACCELERATION = 0.035 
const DECELERATION = 0.2
const TIME_TO_PLACE_TRAP = 0.3
const BASIC_TRAP = 1
const MEAT_BAIT = 2
const VEGGIE_BAIT = 3

var current_trap_selected = BASIC_TRAP

var target_speed = Vector2()
var velocity = Vector2()
var current_animation = null
var next_animation
onready var WalkAnimation = get_node("WalkAnimation")
var BaitThrowNode = preload("res://scenes/Player/BaitThrowNode.tscn")
var trap_scene = preload("res://scenes/Trap/Trap.tscn")
var meat_bait_scene = preload("res://scenes/Trap/MeatBait.tscn")
var veggie_bait_scene = preload("res://scenes/Trap/VeggieBait.tscn")
onready var traps = get_node("/root/Main/Traps")
onready var RayCast = get_node("RayCast2D")
var trap_delay = false
onready var trap_timer = get_node("TrapTimer")
var trap_progress = 0
var mouse_released = true
var new_trap
var is_throwing = false
var about_to_die = false

# Called when the node enters the scene tree for the first time.
func _ready():
	respawn()
	cam.target = self
	get_node("/root/Main/GUI").select_trap("trap")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
		
	
	#increment counters
	if about_to_throw:
		started_throwing += delta
		if started_throwing >= throw_starting_timeout:
			throw_trap(throwing_type)
	
	if placing_trap:
		if Input.is_action_just_released("mouse_click"):
			placing_trap = false

		else:
			
			trap_progress += delta
			if trap_progress > TIME_TO_PLACE_TRAP:
				placeTrap(BASIC_TRAP)
				placing_trap = false
				
			$ProgressBar.value = trap_progress/TIME_TO_PLACE_TRAP
		
	
	$ProgressBar.visible = placing_trap
	
	target_speed = Vector2(0,0)
	next_animation = "idle"
#	if Input.is_action_just_pressed("restart"):
#		pass

	if mounted:
		
		if Input.is_action_just_pressed("mouse_click"):
			unmount()
	elif about_to_die:
		pass
	else:
		
		if Input.is_action_just_pressed("number_1"):
			current_trap_selected = BASIC_TRAP
			get_node("/root/Main/GUI").select_trap("trap")
		elif Input.is_action_just_pressed("number_2"):
			current_trap_selected = MEAT_BAIT
			get_node("/root/Main/GUI").select_trap("meat")
		elif Input.is_action_just_pressed("number_3"):
			current_trap_selected = VEGGIE_BAIT
			get_node("/root/Main/GUI").select_trap("veggie")
		
		if current_trap_selected == BASIC_TRAP:
			if not placing_trap:
				if Input.is_action_just_pressed("mouse_click"):
					start_placing_trap()
				handleInput()
				
		elif current_trap_selected == MEAT_BAIT or current_trap_selected == VEGGIE_BAIT:
			if Input.is_action_just_pressed("mouse_click"):
				if mouse_released and not is_throwing:
					if trap_delay == false:
						is_throwing = true
						placeTrap(current_trap_selected)
			else:
				handleInput()


	
func handle_collision():
	for idx in get_slide_count():
		var collision = get_slide_collision(idx)
		if collision.collider.is_in_group("Dinos"):
			if not mounted:
				if collision.collider.state == collision.collider.STUNNED:
					mount(collision.collider)
					break
				
func mount(dino):
	mounted = true
	dino.mount()
	cam.target = dino
	current_mount = dino
	global_position = Vector2(-2000,-2000)
	
func unmount():
	mounted = false
	global_position = current_mount.global_position
	current_mount.unmount()
	cam.target = self
	current_mount = null
#	player_camera.make_current()

func placeTrap(trap_type):
	new_trap = null
	if trap_type == BASIC_TRAP:
		new_trap = trap_scene.instance()
		new_trap.global_position = global_position
		$Sfx/TrapSound.play()
		traps.add_child(new_trap)
		trap_timer.start()
		trap_delay = true
		trap_progress = 0
		mouse_released = false
	else: start_throwing_trap(trap_type)

func start_throwing_trap(trap_type):
	about_to_throw = true
	started_throwing = 0.0
	throwing_type = trap_type
	
func start_placing_trap():
	$ProgressBar.value = 0
	placing_trap = true
	trap_progress = 0.0	
	
func throw_trap(trap_type):
	var colliding_position = null
	if RayCast.is_colliding():
		colliding_position = RayCast.get_collision_point()
	about_to_throw = false
	$Sfx/ThrowSound.play()
	if trap_type == MEAT_BAIT:
		var animation_node = BaitThrowNode.instance()
		animation_node.global_position = global_position
		get_parent().add_child(animation_node)
		animation_node.connect("animation_finished", self, "_on_BaitThrowAnimation_animation_finished")
		var animation_to_play = ""
		match last_direction:
			"walk_left":
#				animation_node.get_node("BaitThrowAnimation").play("meat_left")
				animation_to_play = "meat_left"
				new_trap = meat_bait_scene.instance()
				if colliding_position:
					new_trap.global_position = colliding_position
				else:
					new_trap.global_position = global_position + Vector2(-80,0)
			"walk_right":
				animation_to_play = "meat_right"
#				animation_node.get_node("BaitThrowAnimation").play("meat_right")
				new_trap = meat_bait_scene.instance()
				if colliding_position:
					new_trap.global_position = colliding_position
				else:
					new_trap.global_position = global_position + Vector2(80,0)
			"walk_up":
				animation_to_play = "meat_up"
#				animation_node.get_node("BaitThrowAnimation").play("meat_up")
				new_trap = meat_bait_scene.instance()
				if colliding_position:
					new_trap.global_position = colliding_position
				else:
					new_trap.global_position = global_position + Vector2(0,-80)
			_:
				animation_to_play = "meat_down"
#				animation_node.get_node("BaitThrowAnimation").play("meat_down")
				new_trap = meat_bait_scene.instance()
				if colliding_position:
					new_trap.global_position = colliding_position
				else:
					new_trap.global_position = global_position + Vector2(0,80)
				
		animation_node.play(animation_to_play)
				
	elif trap_type == VEGGIE_BAIT:
		var animation_node = BaitThrowNode.instance()
		animation_node.global_position = global_position
		get_parent().add_child(animation_node)
		animation_node.connect("animation_finished", self, "_on_BaitThrowAnimation_animation_finished")
		var animation_to_play = ""
		match last_direction:
			"walk_left":
				animation_to_play = "veggie_left"
#				animation_node.get_node("BaitThrowAnimation").play("veggie_left")
				new_trap = veggie_bait_scene.instance()
				if colliding_position:
					new_trap.global_position = colliding_position
				else:
					new_trap.global_position = global_position + Vector2(-80,0)
			"walk_right":
				animation_to_play = "veggie_right"
#				animation_node.get_node("BaitThrowAnimation").play("veggie_right")
				new_trap = veggie_bait_scene.instance()
				if colliding_position:
					new_trap.global_position = colliding_position
				else:
					new_trap.global_position = global_position + Vector2(80,0)
			"walk_up":
				animation_to_play = "veggie_up"
#				animation_node.get_node("BaitThrowAnimation").play("veggie_up")
				new_trap = veggie_bait_scene.instance()
				if colliding_position:
					new_trap.global_position = colliding_position
				else:
					new_trap.global_position = global_position + Vector2(0,-80)
			_:
				animation_to_play = "veggie_down"
#				animation_node.get_node("BaitThrowAnimation").play("veggie_down")
				new_trap = veggie_bait_scene.instance()
				if colliding_position:
					new_trap.global_position = colliding_position
				else:
					new_trap.global_position = global_position + Vector2(0,80)
				
		animation_node.play(animation_to_play)

func _on_TrapTimer_timeout():
	trap_delay = false

func get_hit():
	velocity = Vector2(0,0)
	if not about_to_die:
		about_to_die = true
		$Sfx/HitSound.play()
		$Particles/BloodParticles.emitting=true
	
func handleInput():
	if not about_to_die:
		trap_progress = 0
		mouse_released = true
		if Input.is_action_pressed("ui_right"):
			next_animation = "walk_right"
			target_speed.x = 1
		elif Input.is_action_pressed("ui_left"):
			next_animation = "walk_left"
			target_speed.x = -1
		else:
			target_speed.x = 0
		if Input.is_action_pressed("ui_up"):
			next_animation = "walk_up"
			target_speed.y = -1
		elif Input.is_action_pressed("ui_down"):
			next_animation = "walk_down"
			target_speed.y = 1
		else:
			target_speed.y = 0
			
		if target_speed == Vector2(0,0):
			velocity.x = lerp(velocity.x, target_speed.x, DECELERATION)
			velocity.y = lerp(velocity.y, target_speed.y, DECELERATION)
		else:
			target_speed = target_speed.normalized()*WALK_SPEED
			velocity.x = lerp(velocity.x, target_speed.x, ACCELERATION)
			velocity.y = lerp(velocity.y, target_speed.y, ACCELERATION)
		
		
		
		
	if current_animation != next_animation:
		if !next_animation == "idle":
			last_direction = next_animation
			$Sfx/WalkSound.play()
			current_animation = next_animation
			if current_animation == "walk_right" or current_animation == "walk_left":
				get_node("CollisionShapeUpDown").disabled = true
				get_node("CollisionShapeLeftRight").disabled = false
				if current_animation == "walk_right":
					RayCast.rotation_degrees = 270
				else:
					RayCast.rotation_degrees = 90
			elif current_animation == "walk_up" or current_animation == "walk_down":
				get_node("CollisionShapeUpDown").disabled = false
				get_node("CollisionShapeLeftRight").disabled = true
				if current_animation == "walk_up":
					RayCast.rotation_degrees = 180
				else:
					RayCast.rotation_degrees = 0
			WalkAnimation.play(current_animation)
		else:
			current_animation = "null"
			WalkAnimation.stop()
			$Sfx/WalkSound.stop()
		
		
	
	move_and_slide(velocity)
	handle_collision()

func _on_BaitThrowAnimation_animation_finished():
	is_throwing = false
	traps.add_child(new_trap)
	trap_timer.start()
	trap_delay = true
	trap_progress = 0
	mouse_released = false

func _on_HitSound_finished():
	
	$Particles/BloodParticles.emitting=false
	respawn()

func respawn():
	global_position = get_node("/root/Main/SpawnPosition").global_position
	yield(get_tree().create_timer(DEATH_TIMEOUT),"timeout")
	about_to_die=false
