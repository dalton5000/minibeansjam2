extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var trap_type
var disabled = false setget set_disabled
onready var sprite = get_node("Sprite")
onready var shape = get_node("CollisionShape2D")

# Called when the node enters the scene tree for the first time.
func _ready():
	if has_node("Veggie"):
		trap_type = "veggie_bait"
	elif has_node("Meat"):
		trap_type = "meat_bait"
	else:
		trap_type = "basic"
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Trap_body_entered(body):
	if body.is_in_group("Dinos"):
		body.hit_trap(trap_type, self)


func set_disabled(flag):
	if flag:
		$SnapSound.play()
	sprite.hide()
	shape.disabled = flag
	yield(get_tree().create_timer(5.5),"timeout")
	queue_free()