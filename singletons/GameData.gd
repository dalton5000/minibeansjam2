extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var RescueZone
var PointsLabel
var bodies_in_zone
var score = 0
var max_score = 15

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func update_score():
	if !RescueZone:
		RescueZone = get_node("/root/Main/Level/RescueZone")
	if !PointsLabel:
		PointsLabel = get_node("/root/Main/GUI/VBoxContainer/Label")
	score = 0
	bodies_in_zone = RescueZone.get_overlapping_bodies()
	for body in bodies_in_zone:
		if body.is_in_group("Dinos"):
			score = score + 1
	PointsLabel.text = String(score) + " dinos are safe"
	if score >= max_score:
		PointsLabel.text = "You saved all the dinos! Hurray!"